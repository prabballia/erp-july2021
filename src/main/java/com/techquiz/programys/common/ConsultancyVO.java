package com.techquiz.programys.common;

public class ConsultancyVO {

	private int cid;
	private String name;
	private String description;
	private String ccode;

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCcode() {
		return ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	@Override
	public String toString() {
		return "ConsultancyVO [cid=" + cid + ", name=" + name + ", description=" + description + ", ccode=" + ccode
				+ "]";
	}

}
