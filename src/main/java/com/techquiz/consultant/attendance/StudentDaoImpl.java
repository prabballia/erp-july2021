/**
 * This is implementation class for students functionality
 */
package com.techquiz.consultant.attendance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.techquiz.consultant.attendance.vo.AttendanceBean;
import com.techquiz.consultant.attendance.vo.ConsultantAttendanceVO;
import com.techquiz.consultant.attendance.vo.StudentAttendanceVO;
import com.techquiz.consultant.attendance.vo.StudentListVO;
import com.techquiz.utils.TechnohunkConnectionPool;

/**
 * @author Nagen
 * 
 */

@Repository
public class StudentDaoImpl implements StudentDao {

	public String checkStudentId(String upTechRoll, String pass) {
		String password = null;
		String ask0 = null;
		ResultSet rs = null;
		try (Connection connection = TechnohunkConnectionPool.getDbConnection();
				Statement stmt = connection.createStatement();) {
			rs = stmt.executeQuery("select password from stu_pass where upturollno=" + Long.parseLong(upTechRoll));
			if (rs.next()) {
				password = (rs.getString(1));
			}
			if (password != null && password.equals(pass)) {
				ask0 = "good";
			} else {
				ask0 = "failure";
			}
		} catch (SQLException exe) {
			System.out.println("This is exception.." + exe.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception exe) {
				exe.printStackTrace();
			}
		}
		return ask0;
	}

	public String insertCollegeFeedBack(String uptuRollNo, int canteen, int hostels, int library, int laboratories,
			int games, int security, int housekeeping, int transportaion, int tap, int medical, String remarks) {
		String ans = null;
		int hit = 0;
		try (Connection connection = TechnohunkConnectionPool.getDbConnection();
				Statement stmt = connection.createStatement();) {
			hit = stmt.executeUpdate("insert into college_feedback values('" + uptuRollNo + "'," + canteen + ","
					+ hostels + "," + library + "," + laboratories + "," + games + "," + security + "," + housekeeping
					+ "," + transportaion + "," + tap + "," + medical + ",'" + remarks + "')");
			// System.out.println(hit);
			if (hit > 0) {
				ans = "good";
			} else {
				ans = "failure";
			}
		} catch (SQLException exe) {
		    System.out.println("This is exception.." + exe.getMessage());
		}		return ans;
	}

	public String insertTeacherFeedBack(String stsem, String id, int t, String emp_record[][]) {
		String ans1 = null;
		int hit = 0;
		try (Connection connection = TechnohunkConnectionPool.getDbConnection();
				Statement stmt = connection.createStatement();) {
			// ans1=studentDao.insertTeacherFeedBack(Integer.parseInt(emp_record[j][0]),stsem,id,Integer.parseInt(emp_record[j][1]),Integer.parseInt(emp_record[j][2]),Integer.parseInt(emp_record[j][3]),Integer.parseInt(emp_record[j][4]),Integer.parseInt(emp_record[j][5]),Integer.parseInt(emp_record[j][6]));

			for (int j = 0; j < t; j++) {
				hit = stmt.executeUpdate("insert into teacher_feedback values(" + Integer.parseInt(emp_record[j][0])
						+ ",'" + stsem + "','" + id + "'," + Integer.parseInt(emp_record[j][1]) + ","
						+ Integer.parseInt(emp_record[j][2]) + "," + Integer.parseInt(emp_record[j][3]) + ","
						+ Integer.parseInt(emp_record[j][4]) + "," + Integer.parseInt(emp_record[j][5]) + ","
						+ Integer.parseInt(emp_record[j][6]) + ")");
			}
			// System.out.println(hit);

			if (hit > 0) {
				ans1 = "good";
			} else {
				ans1 = "failure";
			}

		}

		catch (SQLException exe) {
			System.out.println("This is exception.." + exe.getMessage());
		}

		return ans1;

	}
	
	@Override
	public List<ConsultantAttendanceVO> findConsultantAttendance(String upTechRoll) throws SQLException {
		List<ConsultantAttendanceVO> attendanceVOs = new ArrayList<>();
		ResultSet rs = null;
		try (Connection connection = TechnohunkConnectionPool.getDbConnection();
				Statement stmt = connection.createStatement();) {
			String systemKey = "";
			rs = stmt.executeQuery("select batch from consultants_tbl where empid='" + upTechRoll + "'");
			if (rs.next()) {
				systemKey = rs.getString(1);
			}

			rs = stmt.executeQuery("select DOCLASS from attendances where AB_ROLLS like '%" + upTechRoll + "%'");
			List<String> docList = new ArrayList<>();
			while (rs.next()) {
				docList.add(rs.getString(1));
			}

			rs = stmt.executeQuery(
					"select SYSTEM_KEY as systemKey,DOCLASS,AB_ROLLS as absRolls,EMP_CODE as userId,DAY_PERIOD as cday,TOPIC_DESCRIPTION as td from attendances where SYSTEM_KEY like  '%"
							+ systemKey + "%'");
			while (rs.next()) {
				String csystemKey = rs.getString(1);
				ConsultantAttendanceVO attendanceVO = new ConsultantAttendanceVO();
				attendanceVO.setClassName(csystemKey);
				String doclass = rs.getString(2);
				attendanceVO.setDoc(doclass);
				String absRoll = rs.getString(3);
				attendanceVO.setStatus(absRoll.contains(upTechRoll) ? "A" : "P");
				attendanceVO.setTrainerName(rs.getString(4));
				attendanceVO.setDay(rs.getString(5));
				attendanceVO.setTopic(rs.getString(6));
				attendanceVOs.add(attendanceVO);
			}
		}
		return attendanceVOs;
	}

	public ArrayList<StudentAttendanceVO> findStudentAttendance(String upTechRoll) {
		ArrayList<StudentAttendanceVO> studentAttendanceList = new ArrayList<StudentAttendanceVO>();
		ResultSet rs1=null;;
		ResultSet rs=null;
			try (Connection connection = TechnohunkConnectionPool.getDbConnection();
					Statement stmt = connection.createStatement();	Statement stmt1 = connection.createStatement();) {
			
			// accessing branch,semester,section on the basis of up tech roll
			String systemKey = "";
			 rs = stmt.executeQuery("select batch from consultants_tbl where empid='" + upTechRoll + "'");
			if (rs.next()) {
				systemKey = rs.getString(1);
			}
			// calculating the number of classes held for all subjects
			rs = stmt.executeQuery(
					"select system_key, count(*) FROM (SELECT system_key from ATTENDANCES   WHERE system_key LIKE '%"
							+ systemKey + "%' ) as T group by system_key order by system_key");
			// calculating the number of classes attended
			 rs1 = stmt1.executeQuery(
					"select system_key,count(*) from (select system_key,ab_rolls from attendances where system_key like '"
							+ systemKey + "%' and ab_rolls like '%" + upTechRoll
							+ "%') as P group by system_key  order by system_key");

			String classesAttended = "0";
			String classesAbsent = "0";
			while (rs.next()) {
				String sysKey = rs.getString(1);
				String tokens[] = sysKey.split("-");
				sysKey = tokens[tokens.length - 2] + "-" + tokens[tokens.length - 1];
				String classesHeld = rs.getString(2);
				if (rs1.next()) {
					classesAbsent = rs1.getString(2);
				}
				/*
				 * ResultSet rs2 = stmt
				 * .executeQuery("select F_SHORT_SUBJECT_NAME from subject_assignment where sub_code like '"
				 * + sysKey + "'");
				 */
				String shortSubName = systemKey.split("-")[2];
				classesAttended = (Integer.parseInt(classesHeld) - Integer.parseInt(classesAbsent)) + "";

				double per = Math
						.round(((double) Integer.parseInt(classesAttended) / Integer.parseInt(classesHeld)) * 100);

				studentAttendanceList.add(new StudentAttendanceVO(shortSubName, classesHeld, classesAttended,
						classesAbsent, per, sysKey));
			}
		} catch (SQLException exe) {
			System.out.println("This is exception.." + exe.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
					if (rs1 != null)
						rs1.close();
				}
			} catch (Exception exe) {
				exe.printStackTrace();
			}
		}	
		return studentAttendanceList;
	}

	/**
	 * Fetching all the list of student for particular branch section changes
	 * and subject is not taken into the consideration;
	 * 
	 */
	@Override
	public List<StudentListVO> getStudentList(AttendanceBean form) {
		ResultSet rs = null;
		// SELECT CONCAT(v.first_name ,' ',v.middle_name,' ',v.last_name) FROM
		// students v;
		final String query = "SELECT b.f_lib_id,CONCAT(v.first_name ,' ',v.middle_name,' ',v.last_name),v.email FROM branch_section_changes b, students v  WHERE b.branch =? AND b.semester =?  AND b.section=? AND b.f_lib_id = v.lib_id order by (0 + ROLL)  asc";
		List<StudentListVO> studentList = new ArrayList<StudentListVO>();
		int roll = 1;
			try (Connection connection = TechnohunkConnectionPool.getDbConnection();
					Statement stmt = connection.createStatement();	PreparedStatement pstmt = connection.prepareStatement(query);) {
			pstmt.setString(1, form.getBranch());
			pstmt.setString(2, form.getSemester());
			pstmt.setString(3, form.getSection());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				studentList.add(new StudentListVO(rs.getString(1), rs.getString(2), "" + roll, rs.getString(3)));
				roll++;
			} // end of while
		} catch (SQLException exe) {
			// System.out.println("This is exception.."+exe.getMessage());
			exe.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception exe) {
				exe.printStackTrace();
			}
		}
		if (roll == 1) {
			studentList = null;
		}
		return studentList;
	}
}
