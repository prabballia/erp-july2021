package com.techquiz.trainer.web.controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.synergisitic.it.navigation.ConsultantNavigationPage;
import com.synergisitic.it.util.ApplicationContant;
import com.synergisitic.it.util.DateUtils;
import com.synergisitic.it.web.form.UserId;
import com.techquiz.constant.AttendanceConstant;
import com.techquiz.constant.CMSApplicationConstant;
import com.techquiz.consultant.attendance.AttendanceDao;
import com.techquiz.consultant.attendance.StudentDao;
import com.techquiz.consultant.attendance.vo.AttendanceBean;
import com.techquiz.consultant.attendance.vo.AttendanceStatusVO;
import com.techquiz.consultant.attendance.vo.ConsultantAttendanceVO;
import com.techquiz.consultant.attendance.vo.StudentListVO;
import com.techquiz.consultant.attendance.vo.UserApplicationMessage;
import com.techquiz.trainer.service.impl.IConsultantAssesmentService;
import com.techquiz.trainer.web.controller.vo.ConsultantsVO;

@Controller
@RequestMapping("/attendance")
public class AttendanceController {

	@Autowired
	private AttendanceDao attendanceDao;
	
	@Autowired
	private StudentDao studentDao;

	@Autowired
	@Qualifier("ConsultantAssesmentService")
	private IConsultantAssesmentService consultantAssesmentService;

	private final static String SHOW_ATTENDANCE_PAGE = "showAttendanceStatus";
	private final static String SHOW_LAB_ATTENDANCE_PAGE = "showLabAttendanceStatus";

	/**
	 * 
	 * @param dbDate
	 * @param attendanceStatusVOs
	 * @return
	 * @throws ParseException
	 */
	private String calculateEditableDates(String dbDate) throws ParseException {
		SimpleDateFormat fromUser = new SimpleDateFormat("dd-MMM-yy");
		Date old = fromUser.parse(dbDate);
		Date cdate = new Date();
		long diff = DateUtils.dayDiff(cdate, old);
		if (diff <= AttendanceConstant.ATTENDACE_CAN_BE_EDITED) {
			return "yes";
		} else {
			return "no";
		}
	}
	
	@RequestMapping("/consultant/details")
	public String getStudentDetails(@RequestParam String consultantId , Model model) throws SQLException{
		List<ConsultantAttendanceVO> consultantAttendanceVOs=studentDao.findConsultantAttendance(consultantId);
		ConsultantsVO consultantsVO=consultantAssesmentService.findConsultantByEmpId(consultantId);
		int totalAbsent=0;
		for(ConsultantAttendanceVO attendanceVO:consultantAttendanceVOs){
			if(attendanceVO.getStatus().equalsIgnoreCase("A")){
				totalAbsent++;
			}
		}
		int classAttended=consultantAttendanceVOs.size()-totalAbsent;
		DecimalFormat df2 = new DecimalFormat("#.##");
		double attPer=(((double)classAttended)/consultantAttendanceVOs.size())*100;
		String strattPer=df2.format(attPer);
		model.addAttribute("consultantAttendanceVOs",consultantAttendanceVOs);
		model.addAttribute("attPer",strattPer);
		model.addAttribute("classAttended",classAttended);
		model.addAttribute("consultantsVO",consultantsVO);
		model.addAttribute("totalAbsent",totalAbsent);
		return ConsultantNavigationPage.TRAINER_BASE + "consultant-attendance-details";
	}

	@RequestMapping(value = "consultant-list", method = RequestMethod.GET)
	public String showActiveBatch(Model model) {
		List<String> batchList = consultantAssesmentService.findActiveBatches();
		model.addAttribute("batchList", batchList);
		model.addAttribute("nextAction", "startConsultantInterview");
		model.addAttribute("pageTitle", "Consultant Attendance Page");
		model.addAttribute("nextTitle", "Start");
		return ConsultantNavigationPage.TRAINER_BASE + "attendance-consultant-list";
	}

	@RequestMapping(value = "view-consultant-list", method = RequestMethod.GET)
	public String viewAttendanceList(Model model) {
		List<String> batchList = consultantAssesmentService.findActiveBatches();
		model.addAttribute("batchList", batchList);
		model.addAttribute("nextAction", "startConsultantInterview");
		model.addAttribute("pageTitle", "Consultant View Attendance Page");
		model.addAttribute("nextTitle", "Start");
		return ConsultantNavigationPage.TRAINER_BASE + "view-attendance-consultant-list";
	}

	@RequestMapping(value = "/save/success", method = RequestMethod.POST)
	public String saveSuccess(Model model) {
		model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
				"Your Attendance is marked into database successfully.");
		return ConsultantNavigationPage.TRAINER_BASE + "success";
	}

	@RequestMapping(value = "/save/success", method = RequestMethod.GET)
	public String saveGetSuccess(Model model) {
		model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
				"Your Attendance is marked into database successfully.");
		return ConsultantNavigationPage.TRAINER_BASE + "success";
	}

	
	@RequestMapping(value = "/showStatus", method = RequestMethod.GET)
	public String getAttendanceForEdit(@RequestParam(value = "cname", required = false) String cname,
			@RequestParam("batchName") String batch, @RequestParam("doc") String doc,@RequestParam(value = "op", required = false) String operation ,HttpSession session,
			Model model) {
		final String SHOW_STUDENT_LIST = "showStudentList";
		String unit = "VI";
		String period = "4";
		AttendanceBean attendanceBean = new AttendanceBean();
		attendanceBean.setUnit(unit);
		// G-KUEBIKO-JAN-2012-FS
		attendanceBean.setBranch("Q");
		attendanceBean.setSemester("Java");
		attendanceBean.setSection(batch);
		attendanceBean.setSubCode("TH");
		// NEW CODE TO CHECK WEATHER THIS ATTENDANCE IS FOR LAB OR LECTURE!
		String labName = "LECTURE";
		attendanceBean.setSubjectLab(true);
		attendanceBean.setLabName(labName);

		// adding date into request scope to edit the functionality.
		model.addAttribute("doclass", doc);
		// making date mysql compatible
		doc = DateUtils.getDateOfClass(doc);
		attendanceBean.setPeriod(period);
		attendanceBean.setUnit(unit);

		List<StudentListVO> studentList = attendanceDao.getStudentList(attendanceBean);
		if (studentList != null) {
			java.util.Collections.sort(studentList);
		}
		model.addAttribute("studentList", studentList);
		List<String> absRollsList = attendanceDao.getStudentListForUpdate(attendanceBean, doc);
		String topicDescription=attendanceDao.getTopicByClass(attendanceBean, doc);
		model.addAttribute("topicDescription", topicDescription);
		// //System.out.println("HHHH...."+absRollsList+request.getParameter("update"));
		model.addAttribute("absRollsList", absRollsList);
		model.addAttribute("topicDescription", topicDescription);
		// //System.out.println(absRollsList);
		int countPresent = 0;
		for (StudentListVO studentListVO : studentList) {
			if (!absRollsList.contains(studentListVO.getUpTechRoll())) {
				countPresent++;
			}
		}
		model.addAttribute("pageTitle", "Consultant Attendance Status Page");
		model.addAttribute("countPresent", countPresent);
		if (studentList != null) {
			model.addAttribute("countAbsent", studentList.size() - countPresent);
		}
		 return ConsultantNavigationPage.TRAINER_BASE + "view-edit-consultant-attendance";
	}

	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public String viewAttendance(@RequestParam(value = "cname", required = false) String cname,
			@RequestParam("batchName") String batch, HttpSession session, Model model) throws ParseException {
		String unit = "VI";
		String period = "4";
		AttendanceBean attendanceBean = new AttendanceBean();
		attendanceBean.setUnit(unit);
		// G-KUEBIKO-JAN-2012-FS
		attendanceBean.setBranch("Q");
		attendanceBean.setSemester("Java");
		attendanceBean.setSection(batch);
		attendanceBean.setSubCode("TH");
		// NEW CODE TO CHECK WEATHER THIS ATTENDANCE IS FOR LAB OR LECTURE!
		String labName = "LECTURE";
		attendanceBean.setSubjectLab(true);
		attendanceBean.setLabName(labName);
		String sysdate = DateUtils.getCurrentDate();
		sysdate = DateUtils.getDateOfClass(sysdate);
		ArrayList<AttendanceStatusVO> attendanceStatusVO = attendanceDao.showAttendanceStatus(attendanceBean, sysdate);
		if (attendanceStatusVO == null || attendanceStatusVO.size() == 0) {
			model.addAttribute("applicationMessage", "No Attendance is marked for this class so far.");
			return "redirect:/action/attendance/save/success";
		}
		float totalClassAvgAttendance = 0.0F;
		for (AttendanceStatusVO asvo : attendanceStatusVO) {
			totalClassAvgAttendance = totalClassAvgAttendance + asvo.getPercentageAttInClass();
			String yesno = calculateEditableDates(asvo.getDateOfClass());
			asvo.setShow(yesno);
		}

		List<String> batchList = consultantAssesmentService.findActiveBatches();
		model.addAttribute("batchList", batchList);
		model.addAttribute("nextAction", "startConsultantInterview");
		model.addAttribute("pageTitle", "Consultant View Attendance Page");
		totalClassAvgAttendance = totalClassAvgAttendance / attendanceStatusVO.size();
		model.addAttribute("totalStudentInClass", attendanceStatusVO != null && attendanceStatusVO.size() > 0
				? attendanceStatusVO.get(0).getTotalStudent() : 0);
		model.addAttribute("totalClassAvgAttendance", totalClassAvgAttendance);
		model.addAttribute("attendanceStatusVO", attendanceStatusVO);
		model.addAttribute("viewForm", attendanceBean);
		model.addAttribute("systemkey", attendanceBean.getBranch() + "-" + attendanceBean.getSemester() + "-"
				+ attendanceBean.getSection() + "-" + attendanceBean.getSubCode());
		return ConsultantNavigationPage.TRAINER_BASE + "view-consultant-attendance";
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String saveAttendance(@RequestParam("cname") String cname, @RequestParam("batchName") String batch,
			@RequestParam("status") String[] status, @RequestParam("topicDescription") String topicDescription,
			@RequestParam(value = "doclass", required = false) String calenderDate,
			@RequestParam("operation") String operationName, HttpSession session, Model model) throws ParseException {
		// Setting the unit name and description
		String unit = "VI";
		String period = "4";

		List<ConsultantsVO> consultantsVOList = consultantAssesmentService.findConsultantsByBatch(batch);

		String[] allRolls = new String[consultantsVOList.size()];
		int index = 0;
		for (ConsultantsVO consultantsVO : consultantsVOList) {
			allRolls[index] = consultantsVO.getEmpid();
			index++;
		}

		AttendanceBean attendanceBean = new AttendanceBean();
		attendanceBean.setUnit(unit);
		attendanceBean.setTopicDescription(topicDescription);
		// G-KUEBIKO-JAN-2012-FS
		attendanceBean.setBranch("Q");
		attendanceBean.setSemester("Java");
		attendanceBean.setSection(batch);
		attendanceBean.setSubCode("TH");
		// 14-05-12
		/*
		 * String doclass = attendanceBean.getDd() + "-" +
		 * attendanceBean.getMm() + "-" + attendanceBean.getYy();
		 */
		String doclass = DateUtils.getJavaCurrentDateYYMMDDFormat();

		// fetching period number from session which one is old
		String speriodNo = attendanceBean.getPeriod();
		// updating new period into the session.
		if (period != null)
			attendanceBean.setPeriod(period);

		UserId userId = (UserId) session.getAttribute(ApplicationContant.USER_SESSION_DATA);
		String empCode = userId.getLoginId();

		// String sysdate =DateUtils.getCurrentDate();
		// String sysdate =DateUtils.getCurrentDateSQLDB();
		// java.sql.Date sysdate = new
		// java.sql.Date(System.currentTimeMillis());
		java.util.Date date = new Date();
		Timestamp sysdate = new Timestamp(date.getTime());

		// stmnt.setDate(1, date);
		if (status == null) {
			status = new String[] {};
		}
		List<String> statusRollList = Arrays.asList(status);
		StringBuilder absRolls = new StringBuilder();

		int noOfAbsentStudent = 0;
		for (int i = 0; i < allRolls.length; i++) {
			if (!statusRollList.contains(allRolls[i])) {
				absRolls.append(allRolls[i] + "-");
				noOfAbsentStudent++;
			}
		}

		String absentStudents = noOfAbsentStudent + "";
		// //System.out.println("THIS IS TYPE OF
		// OPERATION"+request.getParameter("operation"));
		// AttendanceDao attendanceDao = new AttendanceDaoImpl();
		// This is updated because of doclass should be also update

		if (status != null && status.length == 0) {
			model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
					"Sorry , you cannot submit blank attendance!!");
			return "redirect:/action/attendance/consultant-list";
		}

		if ("update".equals(operationName)) {
			// String calenderDate=request.getParameter("doclass");
			/*boolean isDateCorrect = DateUtils.validateCurrentDate(calenderDate);
			if (!isDateCorrect) {
				model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
						"Sorry , you cannot update doclass for future date (" + calenderDate + "), please check it.");
				return "redirect:/attendance/consultant-list";
			}
			calenderDate = DateUtils.convertMMDDYYYYInToYYYYMMDD(calenderDate);*/
			attendanceBean.setDatepicker(calenderDate);
			//09-Jun-21
			//2021-06-07
			
			doclass=calenderDate;
		}

		/*
		 * if("NEC-101".equalsIgnoreCase(attendanceBean.getSubCode()) &&
		 * 1085==empCode){ empCode=1234; }
		 */

		// Default lecture type will be Theory class
		if (attendanceBean != null && attendanceBean.getTemp() != null) {
			// MCA-IV-A@.NET TRAINING@MCAT-002@LECTURE
			String tempTokens[] = attendanceBean.getTemp().split("@");
			String labName = tempTokens[3];
			attendanceBean.setLabName(labName);
		} else {
			if (attendanceBean.getLabName() == null) {
				attendanceBean.setLabName(CMSApplicationConstant.SUBJECT_TYPE_LECTURE);
			}
		}

		String[] morePeriods = {};
		String result = attendanceDao.saveAttendance(attendanceBean, doclass, period, empCode, absentStudents,
				absRolls.toString(), sysdate, operationName, morePeriods);
		// String operationName=request.getParameter("operation");
		if ("aexist".equalsIgnoreCase(result)) {
			model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
					"You have already filled this attendance, please check it.");
			return "redirect:/action/attendance/consultant-list";
		}
		if (operationName != null && "update".equals(operationName)) {
			model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
					"Your Attendance is updated into database successfully.");
			 return "redirect:/action/attendance/save/success";
		} else {
			model.addAttribute(UserApplicationMessage.APPLICATION_MESSAGE,
					"Your Attendance is marked into database successfully.");
			return "redirect:/action/attendance/save/success";
		}
		// return mapping.findForward(result);
	}

}
