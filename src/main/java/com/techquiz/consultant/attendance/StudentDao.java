/**
 * 
 */
package com.techquiz.consultant.attendance;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.techquiz.consultant.attendance.vo.AttendanceBean;
import com.techquiz.consultant.attendance.vo.ConsultantAttendanceVO;
import com.techquiz.consultant.attendance.vo.StudentAttendanceVO;
import com.techquiz.consultant.attendance.vo.StudentListVO;

/**
 * @author nagendra.yadav
 *
 */
public interface StudentDao {
  
	public ArrayList<StudentAttendanceVO> findStudentAttendance(String upTechRoll);
	public String checkStudentId(String upTechRoll,String pass);
	public String insertCollegeFeedBack(String uptuRollNo,int canteen,int hostels,int library,int laboratories,int games,int security,int housekeeping,int transportaion,int tap,int medical,String remarks);
	public String insertTeacherFeedBack(String stsem, String id, int t,String emp_record[][]);
	public List<StudentListVO> getStudentList(AttendanceBean form);
	List<ConsultantAttendanceVO> findConsultantAttendance(String upTechRoll) throws SQLException;
}

