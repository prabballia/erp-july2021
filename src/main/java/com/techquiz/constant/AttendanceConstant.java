package com.techquiz.constant;

/**
 * @author astha 
 */
public interface AttendanceConstant {
	public static String ADD_ATTENDANCE="addAttendance";
	public static String DELETE_ATTENDANCE="deleteAttendance";
	public static String UPDATE_ATTENDANCE="updateAttendance";
	public static String VIEW_ATTENDANCE="viewAttendance";
	public static int ATTENDACE_CAN_BE_EDITED=0;
	public static int ATTENDACE_CAN_BE_FILLED=50;
	public static String ATTENDANCE_LOGIN_TOKEN="$Qxccy5$3eQwertt";

}
