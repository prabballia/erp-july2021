package com.techquiz.codings.service;

import java.util.List;

import com.techquiz.programys.common.ConsultancyVO;

public interface ConsultancyService {

	List<ConsultancyVO> findAll();

}
