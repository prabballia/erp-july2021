package com.techquiz.codings.dao;

import org.springframework.data.repository.CrudRepository;

import com.techquiz.trainer.dao.entity.ConsultancyEntity;

public interface ConsultancyRepository extends CrudRepository<ConsultancyEntity, Integer> {

}
