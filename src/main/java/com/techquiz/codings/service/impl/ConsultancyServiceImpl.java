package com.techquiz.codings.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techquiz.codings.dao.ConsultancyRepository;
import com.techquiz.codings.service.ConsultancyService;
import com.techquiz.programys.common.ConsultancyVO;


@Service
public class ConsultancyServiceImpl implements ConsultancyService{
	
	@Autowired
	private ConsultancyRepository consultancyRepository;
	
	@Override
	public List<ConsultancyVO> findAll() {
		return StreamSupport.stream(consultancyRepository.findAll().spliterator(), false).map(data->{
			ConsultancyVO consultancyVO=new ConsultancyVO();
			BeanUtils.copyProperties(data, consultancyVO);
			return  consultancyVO;
		}).collect(Collectors.toList());
	}

}
