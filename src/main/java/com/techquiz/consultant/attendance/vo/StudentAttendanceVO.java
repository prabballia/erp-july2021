/**
 * Copyright (c) 2013 CCS.  All rights reserved.  Company Confidential.
 */

package com.techquiz.consultant.attendance.vo;

/**
 * @author nagendra.yadav
 * 
 */
public class StudentAttendanceVO {
	
	private String subjectName;
	private String classesHeld;
	private String classesAttended;
	private String classesAbsent;
	private double attendance;
	private String subjectCode;

	public StudentAttendanceVO(String subjectName, String classesHeld,
			String classesAttended, String classesAbsent, double attendance,
			String subjectCode) {
		this.subjectName = subjectName;
		this.classesHeld = classesHeld;
		this.classesAttended = classesAttended;
		this.classesAbsent = classesAbsent;
		this.attendance = attendance;
		this.subjectCode = subjectCode;
	}

	public StudentAttendanceVO(String subjectCode, String classesHeld,
			String classesAttended) {
		this.subjectCode = subjectCode;
		this.classesAttended = classesAttended;
		this.classesHeld = classesHeld;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getClassesHeld() {
		return classesHeld;
	}

	public void setClassesHeld(String classesHeld) {
		this.classesHeld = classesHeld;
	}

	public String getClassesAttended() {
		return classesAttended;
	}

	public void setClassesAttended(String classesAttended) {
		this.classesAttended = classesAttended;
	}

	public String getClassesAbsent() {
		return classesAbsent;
	}

	public void setClassesAbsent(String classesAbsent) {
		this.classesAbsent = classesAbsent;
	}

	public double getAttendance() {
		return attendance;
	}

	public void setAttendance(double attendance) {
		this.attendance = attendance;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

}
