package com.techquiz.consultant.attendance.vo;

public class ConsultantAttendanceVO {

	private String doc;
	private String topic;
	private String className;
	private String day;
	private String status;
	private String trainerName;
	

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ConsultantAttendanceVO [doc=" + doc + ", topic=" + topic + ", className=" + className + ", day=" + day
				+ ", status=" + status + "]";
	}

}
