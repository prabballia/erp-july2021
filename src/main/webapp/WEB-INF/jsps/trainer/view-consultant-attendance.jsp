<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="k" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Google font -->
    <%@include file="/resources.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/trainer-css/trainer.css" type="text/css" media="all">
    <title>${companyName} - View Attendance Page!</title>
    <style>
input[type=checkbox]{
  display:none;
}

input[type=checkbox] + label:before{
  content: '';
  border-radius: 10px;
  width: 16px;
  height:16px;
  background:white;
  color:blue;
  border: 1px solid black;
  line-height: 15px;
  text-align: center;
  font-size:25px;
  margin-right: 5px;
}

input[type=checkbox]:checked + label:before{
  color: red;
   background:#37abf2;
}
</style>
</head>
<body id="page-top">

<!-- PAGE WRAP -->
<div id="page-wrap">

<div class="body121">
  <div class="main">
      <k:if test="${sessionScope.user_session_data.role=='trainer'}">	
      <%@include file="theader.jsp"%>
      </k:if>
        <k:if test="${sessionScope.user_session_data.role=='admin'}">	
       <%@include file="/aheader.jsp"%>
       </k:if>
  </div>
</div>

    <!-- PROFILE FEATURE -->
    <%@include file="profile-feature.jsp" %>
    <!-- END / PROFILE FEATURE -->


    <!-- CONTEN BAR -->
     <%@include file="content-bar.jsp" %>
   <!-- END / CONTENT BAR -->
 
    <!-- COURSE CONCERN -->
    <section class="quizz-intro-section" style="padding-top: 0px;padding-bottom:10px;background-color: #eee;min-height: 600px;" id="content">
    <form id="viewAttendanceFrom"
							action="${pageContext.request.contextPath}/action/attendance/view"
							method="post">	
			<input type="hidden" name="operation" value="save"/>				
<!--   <section style="padding-top: 0px;padding-bottom:10px;background-color: white;" id="quizz-intro-section" class="quizz-intro-section learn-section"> -->
        <div class="container">
            <div class="title-ct">
                 &nbsp;&nbsp;<h3 style="color: black;font-size: 18px;"><img src="${pageContext.request.contextPath}/images/configure.png" width="25px;"/> - <b> ${pageTitle}</b></h3> 
            </div>
            
            <table class="table table-bordered" width="60%">
      			 	 <tbody>
      			 	 <tr height="25px" >
					     <td colspan="1" align="left" valign="bottom" >
					     
					           <label for="sel1" style="font-family: 'Lato', sans-serif;font-size: 17px;">Org</label>
								 &nbsp;&nbsp;
								  <select class="form-control" name="cname" id="cname" style="width: 250px;display: inline;">
								 	<option>Select Consultancy</option>
								 			<option value="Reb3Tech"  ${param.cname=='Reb3Tech' ? 'selected':''}>Rab3 Tech</option>
								 			<option value="CubicTech"  ${param.cname=='CubicTech'? 'selected':''}>Cubic Tech</option>
								 			<option value="Kuebiko"    ${param.cname=='Kuebiko'?'selected':''}>Kuebiko</option>
								 </select>
								 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					     
					           <label for="sel1" style="font-family: 'Lato', sans-serif;font-size: 17px;">Select Batch:</label>
								 &nbsp;&nbsp;
								  <select class="form-control" name="batchName" id="batchName" style="width: 200px;display: inline;">
								 	<option>Select Batch</option>
								 		<c:forEach items="${batchList}" var="item">
								 			<option  ${param.batchName==item? 'selected':''}>${item}</option>
								 		</c:forEach>
								 </select>
								  &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
								 <a
										href="javascript:viewAttendance();">
										<input type="button" value="Show Attendance" class="btn btn-success"
										style="font-family: 'Lato', sans-serif; font-size: 17px;;" />
									</a>
		
					     </td>
					        <td colspan="1" align="left" valign="bottom" >
					     </td>
					   </tr>
					  </tbody>
					  
					 </table>
 <hr style="color: blue"/>
					   <img src="${pageContext.request.contextPath}/images/users.png" style="height: 34px;"/> 
					    <span style="font-family: 'Lato', sans-serif;font-size: 16px;">All Attendance List:-</span>
					    <div style="float: right;">
					    <img src="${pageContext.request.contextPath}/images/kick.png" height="30px" style="font-family: 'Lato', sans-serif;font-size: 17px;">   Total Classes : <b><span id="totalPresent">${fn:length(attendanceStatusVO)}</span></b>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <img src="${pageContext.request.contextPath}/images/favicon.ico" height="30px" style="font-family: 'Lato', sans-serif;font-size: 17px;">   Total Consultant : <b><span id="totalRecords">${totalStudentInClass}</span></b>
                             </div> 
            <div class="table-student-submission">
            	<table class="table table-bordered" width="100%" id="theader">
						 <tbody id="tableContent">					   
					     <tr height="30px" style="color: white;background-color:#607D8B;;vertical-align: middle;" align="left">
					     <td width="10px"><b>Sno.</b></td>
					       <td width="100px;"><b>Date Of Class.</b></td>
					   	   <td><b>Absent Student(s)</b></td>
					   	   <td width="50px"><b>Total Absent</b></td>
					   	    <td width="50px"><b>Att%</b></td>
					   	    <td width="100px">
					   	    <b>Action</b></td>
					     </tr>
		

<c:forEach items="${attendanceStatusVO}" var="item" varStatus="toto">		
					
	<tr >
					      <tr height="25px" style="color: black;font-size:16px;">
					     <td>&nbsp;${toto.count}</td>
					   	   <td>&nbsp;${item.dateOfClass}</td>
					   	   <td>&nbsp;${item.absentRolls}</td>
					   	    <td>&nbsp;${item.totalAbsentStudent}</td>
							 <td align="center">
							  <b>
							 <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${item.percentageAttInClass}"/>
							 </b>
							</td>
							 <td align="center">
							  <a
										href="${pageContext.request.contextPath}/action/attendance/showStatus?cname=${param.cname}&batchName=${param.batchName}&doc=${item.dateOfClass}&op=view">
										<input type="button" value="View" class="btn btn-primary"
										style="font-family: 'Lato', sans-serif; font-size: 17px;;" />
									</a> 
							 <a
											href="${pageContext.request.contextPath}/action/attendance/showStatus?cname=${param.cname}&batchName=${param.batchName}&doc=${item.dateOfClass}&op=edit">
										<input type="button" value="Edit" class="btn btn-danger"
										style="font-family: 'Lato', sans-serif; font-size: 17px;;" />
									</a> 
							</td>
					     </tr>
					     </c:forEach>
					    <tr height="30px" style="vertical-align: middle;" align="center">
					     <td width="10px">&nbsp;</td>
					      <td width="10px">&nbsp;</td>
					   	   <td width="300px">&nbsp;</td>
					   	   <td>&nbsp;</td>
					   	    <td>&nbsp;</td>
					   	    <td>
					   	    <b>&nbsp;</b></td>
					     </tr>
					        <tr height="30px" style="vertical-align: middle;" align="center">
					     <td width="10px">&nbsp;</td>
					      <td width="10px">&nbsp;</td>
					   	   <td width="300px">&nbsp;</td>
					   	   <td>&nbsp;</td>
					   	    <td>&nbsp;</td>
					   	    <td>
					   	    <b>&nbsp;</b></td>
					     </tr>
					     
					        <tr height="30px" style="vertical-align: middle;" align="center">
					     <td width="10px">&nbsp;</td>
					      <td width="10px">&nbsp;</td>
					   	   <td width="300px">&nbsp;</td>
					   	   <td>&nbsp;</td>
					   	    <td>&nbsp;</td>
					   	    <td>
					   	    <b>&nbsp;</b></td>
					     </tr>
					     
					        <tr height="30px" style="vertical-align: middle;" align="center">
					     <td width="10px">&nbsp;</td>
					      <td width="10px">&nbsp;</td>
					   	   <td width="300px">&nbsp;</td>
					   	   <td>&nbsp;</td>
					   	    <td>&nbsp;</td>
					   	    <td>
					   	    <b>&nbsp;</b></td>
					     </tr>
						</tbody>     
					</table> 
				
            </div>
        </div>
        </form>
        <hr/>  <hr/>    <hr/>  <hr/>
    </section>
    <!-- END / COURSE CONCERN -->


   <!-- FOOTER -->
    <footer id="footer" class="footer">
     <%--   <%@include file="/ffooter.jsp" %> --%>
       <%@include file="/sfooter.jsp" %>
    </footer>
    <!-- END / FOOTER -->



</div>
<!-- END / PAGE WRAP -->

<!-- Load jQuery -->
<!-- Load jQuery -->
  <%@include file="/js.jsp" %>

<script type="text/javascript">

function viewAttendance(){
	
	$("#viewAttendanceFrom").submit();
}



$(document).ready(function() {
    $.each($('.table-wrap'), function() {
        $(this)
            .find('.table-item')
            .children('.thead:not(.active)')
            .next('.tbody').hide();
        $(this)
            .find('.table-item')
            .delegate('.thead', 'click', function(evt) {
                evt.preventDefault();
                if ($(this).hasClass('active')==false) {
                    $('.table-item')
                        .find('.thead')
                        .removeClass('active')
                        .siblings('.tbody')
                            .slideUp(200);
                }
                $(this)
                    .toggleClass('active')
                    .siblings('.tbody')
                        .slideToggle(200);
        });
    });
});
</script>
</body>

</html>